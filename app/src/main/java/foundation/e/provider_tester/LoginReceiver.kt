package foundation.e.provider_tester

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import foundation.e.apps.contract.ParentalControlContract.COLUMN_LOGIN_TYPE

class LoginReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("TAG", "Received login: ${intent?.extras?.getString(COLUMN_LOGIN_TYPE)}")
    }
}
