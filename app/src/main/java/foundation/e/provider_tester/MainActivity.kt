package foundation.e.provider_tester

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp
import foundation.e.apps.contract.ParentalControlContract.COLUMN_LOGIN_TYPE
import foundation.e.apps.contract.ParentalControlContract.COLUMN_PACKAGE_NAME
import foundation.e.apps.contract.ParentalControlContract.PATH_BLOCKLIST
import foundation.e.apps.contract.ParentalControlContract.PATH_LOGIN_TYPE
import foundation.e.apps.contract.ParentalControlContract.getAppLoungeProviderAuthority
import foundation.e.provider_tester.ui.theme.ProviderTesterTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ProviderTesterTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MyLayout(::requestGPlayLogin) { id, stringState ->
                        var result: String
                        CoroutineScope(Dispatchers.Default).launch {
                            stringState.value = ""
                            result = withContext(Dispatchers.IO) {
                                when (id) {
                                    1 -> getLoginInfo()
                                    2 -> getAgeRatings()
                                    else -> "Wrong ID"
                                }
                            }
                            withContext(Dispatchers.Main) {
                                stringState.value = result.takeIf { it.isNotBlank() } ?: "---"
                            }
                        }
                    }
                }
            }
        }
    }

    private val APP_LOUNGE_PKG = "foundation.e.apps${if (BuildConfig.DEBUG) ".debug" else ""}"
    private val AUTHORITY = getAppLoungeProviderAuthority(BuildConfig.DEBUG)

    private val loginTypeUri = Uri.parse("content://${AUTHORITY}/${PATH_LOGIN_TYPE}")
    private val ageRatingUri = Uri.parse("content://${AUTHORITY}/${PATH_BLOCKLIST}")

    private suspend fun getLoginInfo(): String {
        return contentResolver.query(
            loginTypeUri,
            null, null, null, null
        ).use { cursor ->
            if (cursor == null) return@use "null cursor"
            cursor.moveToFirst()
            cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_LOGIN_TYPE))
        }
    }

    private val TAG = this::class.java.name

    private suspend fun getAgeRatings(): String {
        Log.d(TAG, "Start getting age ratings...")
        return contentResolver.query(
            ageRatingUri,
            null, null, null, null
        ).use { cursor ->
            if (cursor == null) return@use "null cursor"
            var appCount = 0
            var content = ""
            while (cursor.moveToNext()) {
                appCount++
                val packageName = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PACKAGE_NAME))
                content += "\n${packageName}"
            }
            Log.d(TAG, "Finish getting age ratings for ${appCount} apps")
            content
        }
    }

    private fun requestGPlayLogin() {
        val intent = packageManager.getLaunchIntentForPackage(APP_LOUNGE_PKG)?.apply {
            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            putExtra("request_gplay_login", true)
        }
        startActivity(intent)
    }
}

@Composable
fun MyLayout(
    requestGPlayLogin: () -> Unit,
    getData: (Int, MutableState<String>) -> Unit,
) {
    val resultToDisplay = remember { mutableStateOf("") }
    var isLoading by remember { mutableStateOf(false) }
    LaunchedEffect(key1 = resultToDisplay.value) {
        if (resultToDisplay.value.isNotBlank()) {
            isLoading = false
        }
    }
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly,
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Button(onClick = {
                isLoading = true
                getData(1, resultToDisplay)
            }) {
                Text(text = "Get login status")
            }
            Button(onClick = {
                isLoading = true
                getData(2, resultToDisplay)
            }) {
                Text(text = "Get blocked apps")
            }
            Button(onClick = {
                requestGPlayLogin()
            }) {
                Text(text = "Request gplay login")
            }
        }
        Column(
            modifier = Modifier.wrapContentWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator(
                modifier = Modifier.graphicsLayer {
                    alpha = if (isLoading) 1F else 0F
                }
            )
            Spacer(modifier = Modifier.size(10.dp))
            TextField(
                value = resultToDisplay.value,
                onValueChange = {},
                readOnly = true,
            )
        }
    }
}